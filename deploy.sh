#!/bin/sh
rm -rf staging/*
ENV="bfl"

echo "Deploying to $ENV"

cd app
npm shrinkwrap
cd ..

rm -rf staging
mkdir staging
cp -r app/public staging/public
cp -r app/views staging/views
cp app/app.js staging
cp app/package.json staging
cp app/npm-shrinkwrap.json staging

cd staging
af update $ENV

cd ..
rm -rf staging
cd app
rm -rf npm-shrinkwrap.json
cd ..


