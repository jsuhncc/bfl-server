var express = require('express'),
    app = express(),
    http = require('http'),
    connect = require('connect'),
    server = http.createServer(app),
    io = require('socket.io').listen(server),
    jade = require('jade');

app.configure(function () {
    app.set('port', process.env.PORT || 3001);
    app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express.logger());
    app.use(express.cookieParser());
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.use(connect.static(__dirname + '/public'));
    app.enable('jsonp callback');
    app.set('view engine', 'jade');
    app.set('views', __dirname + '/views');
});

app.get('/', function (req, res) {
    res.render('chat', { });
});

server.listen(app.get('port'));
console.log("Listening on " + app.get('port'));

var messages = [];
var pseudoArray = []; //block the admin username (you can disable it)
var users = 0; //count the users

io.sockets.on('connection', function (socket) { // First connection
    users += 1; // Add 1 to the count
    reloadUsers(); // Send the count to all the users
    reloadMessages(); // Send the count to all the users
    socket.on('message', function (data) { // Broadcast the message to all
        if (pseudoSet(socket)) {
            var transmit = {date: new Date().toISOString(), pseudo: returnPseudo(socket), message: data};
            messages.push(transmit);
            socket.broadcast.emit('message', transmit);
            console.log("user " + transmit['pseudo'] + " said \"" + data + "\"");
        }
    });
    socket.on('setPseudo', function (data) { // Assign a name to the user
        socket.set('pseudo', data, function () {
            if (pseudoArray.indexOf(data) == -1) {
                pseudoArray.push(data);
                reloadUsers();
            }
            socket.emit('pseudoStatus', 'ok');
            console.log("user " + data + " connected");
            reloadUsers();
        });
    });
    socket.on('disconnect', function () { // Disconnection of the client
        users -= 1;
        reloadUsers();
        if (pseudoSet(socket)) {
            var pseudo;
            socket.get('pseudo', function (err, name) {
                pseudo = name;
            });
            var index = pseudoArray.indexOf(pseudo);
            pseudo.slice(index - 1, 1);
        }
    });
});

function reloadUsers() { // Send the count of the users to all
    io.sockets.emit('nbUsers', pseudoArray);
}

function reloadMessages() { // Send the count of the users to all
    io.sockets.emit('messages', messages);
}

function pseudoSet(socket) { // Test if the user has a name
    var test;
    socket.get('pseudo', function (err, name) {
        if (name == null) test = false;
        else test = true;
    });
    return test;
}
function returnPseudo(socket) { // Return the name of the user
    var pseudo;
    socket.get('pseudo', function (err, name) {
        if (name == null) pseudo = false;
        else pseudo = name;
    });
    return pseudo;
}



